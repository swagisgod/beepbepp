#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <math.h>

#include <switch.h>

#define SAMPLERATE 48000
#define CHANNELCOUNT 2
#define FRAMERATE (1000 / 30)
#define SAMPLECOUNT (SAMPLERATE / FRAMERATE)
#define BYTESPERSAMPLE 2

void fill_audio_buffer(void *audio_buffer, size_t offset, size_t size, int frequency)
{
    if (audio_buffer == NULL)
        return;

    u32 *dest = (u32 *)audio_buffer;
    for (int i = 0; i < size; i++)
    {
        // This is a simple sine wave, with a frequency of `frequency` Hz, and an amplitude 30% of maximum.
        s16 sample = 0.3 * 0x7FFF * sin(frequency * (2 * M_PI) * (offset + i) / SAMPLERATE);

        // Stereo samples are interleaved: left and right channels.
        dest[i] = (sample << 16) | (sample & 0xffff);
    }
}
int MsToNs(int ms)
{
    return ms * 1000000;
}
int main(int argc, char **argv)
{
    Result rc = 0;

    consoleInit(NULL);

    AudioOutBuffer audout_buf;
    AudioOutBuffer *audout_released_buf;

    u32 data_size = (SAMPLECOUNT * CHANNELCOUNT * BYTESPERSAMPLE);
    u32 buffer_size = (data_size + 0xfff) & ~0xfff;

    u8 *out_buf_data = memalign(0x1000, buffer_size);

    if (out_buf_data == NULL)
    {
        rc = MAKERESULT(Module_Libnx, LibnxError_OutOfMemory);
    }

    if (R_SUCCEEDED(rc))
        memset(out_buf_data, 0, buffer_size);

    if (R_SUCCEEDED(rc))
    {
        rc = audoutInitialize();
    }

    if (R_SUCCEEDED(rc))
    {
        rc = audoutStartAudioOut();
    }

    bool play_tone = false;
    int note2play = 0;
    bool StartRace = false;
    printf("Press A to start race!\n");
    printf("Press + to exit.\n");
    while (appletMainLoop())
    {
        hidScanInput();

        u64 kDown = hidKeysDown(CONTROLLER_P1_AUTO);

        if (kDown & KEY_PLUS)
            break;

        if(kDown & KEY_A)
            StartRace = true;

        if (StartRace)
        {
            switch (note2play)
            {
            case 1:
            {
                fill_audio_buffer(out_buf_data, 0, data_size, 1760);
                play_tone = true;
            }
            break;
            case 2:
            {
                fill_audio_buffer(out_buf_data, 0, data_size, 1760);
                play_tone = true;
            }
            break;
            case 3:
            {
                fill_audio_buffer(out_buf_data, 0, data_size, 2000);
                play_tone = true;
            }
            break;
            }

            if (note2play <= 3)
                note2play++;
            else {
                note2play = 0;
                StartRace = false;
                printf("Press A to start race!\n");
            }
        }

        if (R_SUCCEEDED(rc) && play_tone)
        {
            svcSleepThread(MsToNs(1000));
            audout_buf.next = NULL;
            audout_buf.buffer = out_buf_data;
            audout_buf.buffer_size = buffer_size;
            audout_buf.data_size = data_size;
            audout_buf.data_offset = 0;

            audout_released_buf = NULL;

            rc = audoutPlayBuffer(&audout_buf, &audout_released_buf);

            play_tone = false;
        }

        consoleUpdate(NULL);
    }
    rc = audoutStopAudioOut();
    audoutExit();

    consoleExit(NULL);
    return 0;
}
